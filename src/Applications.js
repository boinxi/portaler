const apps = [
  {
    "id": 0,
    "isActive": true,
    "img": "airflow.png",
    "name": "airflow",
    "desc": "Nostrud exercitation tempor ut occaecat ad culpa aliqua id. Deserunt duis elit nisi mollit laboris et sunt occaecat eu eu.",
    "link": "https://www.google.com/"
  },
  {
    "id": 1,
    "isActive": false,
    "img": "consul2.png",
    "name": "consul",
    "desc": "Amet culpa dolor sit dolor esse tempor. Quis tempor qui eu sunt cupidatat proident elit.",
    "link": "https://www.walla.com/"
  },
  {
    "id": 2,
    "isActive": true,
    "img": "gitlab.png",
    "name": "gitlab",
    "desc": "Laborum sint officia amet fugiat tempor do culpa reprehenderit commodo cupidatat. Do magna magna incididunt Lorem enim ipsum.",
    "link": "https://www.google.com/"
  },
  {
    "id": 3,
    "isActive": true,
    "img": "grafana.png",
    "name": "grafana",
    "desc": "Consequat laborum sit ut amet elit irure officia fugiat dolore elit tempor nostrud exercitation magna. Irure velit ex ut laboris voluptate nulla pariatur aliqua esse voluptate.",
    "link": "https://www.google.com/"
  },
  {
    "id": 4,
    "isActive": false,
    "img": "hadoop-elephant.png",
    "name": "hadoop",
    "desc": "Ut voluptate dolor voluptate duis mollit magna in minim qui laboris enim et deserunt. Fugiat laborum qui consequat aliqua excepteur dolor.",
    "link": "https://www.google.com/"
  },
  {
    "id": 5,
    "isActive": true,
    "img": "hue.png",
    "name": "hue",
    "desc": "Reprehenderit laboris Lorem consectetur mollit et velit et commodo non irure reprehenderit ullamco. Quis eu cillum pariatur reprehenderit sit nulla consequat aute qui aliqua proident minim sit do.",
    "link": "https://www.google.com/"
  },
  {
    "id": 6,
    "isActive": true,
    "img": "jfrog2.png",
    "name": "jfrog artifactory",
    "desc": "Veniam deserunt eiusmod eiusmod exercitation tempor mollit dolore in excepteur officia ullamco tempor. Excepteur nulla pariatur elit proident amet duis aliquip.",
    "link": "https://www.google.com/"
  },
  {
    "id": 7,
    "isActive": true,
    "img": "jupyter.png",
    "name": "jupyter",
    "desc": "Est qui cillum irure fugiat incididunt anim ut. Ad et minim proident aliqua occaecat deserunt.",
    "link": "https://www.google.com/"
  },
  {
    "id": 8,
    "isActive": false,
    "img": "kafka.png",
    "name": "kafka",
    "desc": "Eiusmod consectetur incididunt elit est in mollit enim sit nisi. Anim Lorem non veniam excepteur nulla fugiat ut aliqua laboris.",
    "link": "https://www.google.com/"
  },
  {
    "id": 9,
    "isActive": false,
    "img": "magic-wand.png",
    "name": "magic wand",
    "desc": "Ex ad nulla id incididunt do. Voluptate ad sit excepteur cupidatat consequat id exercitation laborum consequat laborum do ad nulla ut.",
    "link": "https://www.google.com/"
  },
  {
    "id": 10,
    "isActive": true,
    "img": "mattermost.png",
    "name": "mattermost chat",
    "desc": "Proident ipsum eiusmod deserunt do. Ea amet amet ullamco et est fugiat est.",
    "link": "https://www.google.com/"
  },
  {
    "id": 11,
    "isActive": true,
    "img": "wikipedia.png",
    "name": "wiki",
    "desc": "Ea est ea tempor tempor. Reprehenderit commodo duis quis in mollit quis veniam pariatur aute pariatur dolor non deserunt culpa.",
    "link": "https://www.google.com/"
  },
  {
    "id": 11,
    "isActive": true,
    "img": "portaler.png",
    "name":"portaler",
    "desc": "Ea est ea tempor tempor. Reprehenderit commodo duis quis in mollit quis veniam pariatur aute pariatur dolor non deserunt culpa.",
    "link": "https://www.google.com/"
  },

]

  export default apps;