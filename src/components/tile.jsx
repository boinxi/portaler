import React from 'react';
import './tile.css';
import Paper from '@material-ui/core/Paper';
import QMark from '../images/q_mark.png'


export default function Tile({ id, isActive, img, name, desc, link, descClick, egg}) {
    return (
        <Paper className="cardWrapper" style={{ backgroundColor:'fff'}}>
            <a href={link} style={{textDecoration:"none", color:"#000"}}>
                <p className="itemText">
                    {name}
                </p>
                <div className={egg ? "egg" : "logoImgWrapper" }>
                    <img src={process.env.PUBLIC_URL + '/items/' + img} className="logoImg" />
                </div>    
            </a>
            <div className="descIcon" onClick={descClick}>
                  <img src={QMark} style={{width:'30px'}}/>
            </div>
        </Paper>
    );
}