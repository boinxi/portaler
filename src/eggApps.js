const apps = [
  {
    "id": 0,
    "isActive": true,
    "img": "amit.png",
    "name": "amit",
    "desc": "Nostrud exercitation tempor ut occaecat ad culpa aliqua id. Deserunt duis elit nisi mollit laboris et sunt occaecat eu eu.",
    "link": "https://www.google.com/"
  },
  {
    "id": 1,
    "isActive": false,
    "img": "dvir.png",
    "name": "dvir",
    "desc": "Amet culpa dolor sit dolor esse tempor. Quis tempor qui eu sunt cupidatat proident elit.",
    "link": "https://www.walla.com/"
  },
  {
    "id": 2,
    "isActive": true,
    "img": "eyal.png",
    "name": "eyal",
    "desc": "Laborum sint officia amet fugiat tempor do culpa reprehenderit commodo cupidatat. Do magna magna incididunt Lorem enim ipsum.",
    "link": "https://www.google.com/"
  },
  {
    "id": 3,
    "isActive": true,
    "img": "golan.png",
    "name": "golan",
    "desc": "Consequat laborum sit ut amet elit irure officia fugiat dolore elit tempor nostrud exercitation magna. Irure velit ex ut laboris voluptate nulla pariatur aliqua esse voluptate.",
    "link": "https://www.google.com/"
  },
  {
    "id": 4,
    "isActive": false,
    "img": "guy.png",
    "name": "guy",
    "desc": "Ut voluptate dolor voluptate duis mollit magna in minim qui laboris enim et deserunt. Fugiat laborum qui consequat aliqua excepteur dolor.",
    "link": "https://www.google.com/"
  },
  {
    "id": 5,
    "isActive": true,
    "img": "idan.png",
    "name": "idan",
    "desc": "Reprehenderit laboris Lorem consectetur mollit et velit et commodo non irure reprehenderit ullamco. Quis eu cillum pariatur reprehenderit sit nulla consequat aute qui aliqua proident minim sit do.",
    "link": "https://www.google.com/"
  },
  {
    "id": 6,
    "isActive": true,
    "img": "ido.png",
    "name": "ido",
    "desc": "Veniam deserunt eiusmod eiusmod exercitation tempor mollit dolore in excepteur officia ullamco tempor. Excepteur nulla pariatur elit proident amet duis aliquip.",
    "link": "https://www.google.com/"
  },
  {
    "id": 7,
    "isActive": true,
    "img": "nir.png",
    "name": "nir",
    "desc": "Est qui cillum irure fugiat incididunt anim ut. Ad et minim proident aliqua occaecat deserunt.",
    "link": "https://www.google.com/"
  },
  {
    "id": 8,
    "isActive": false,
    "img": "ofek.png",
    "name": "ofek",
    "desc": "Eiusmod consectetur incididunt elit est in mollit enim sit nisi. Anim Lorem non veniam excepteur nulla fugiat ut aliqua laboris.",
    "link": "https://www.google.com/"
  },
  {
    "id": 10,
    "isActive": true,
    "img": "or.png",
    "name": "or",
    "desc": "Proident ipsum eiusmod deserunt do. Ea amet amet ullamco et est fugiat est.",
    "link": "https://www.google.com/"
  },
  {
    "id": 11,
    "isActive": true,
    "img": "roy.png",
    "name": "roy",
    "desc": "Ea est ea tempor tempor. Reprehenderit commodo duis quis in mollit quis veniam pariatur aute pariatur dolor non deserunt culpa.",
    "link": "https://www.google.com/"
  },
  {
    "id": 11,
    "isActive": true,
    "img": "yoni.png",
    "name":"yoni",
    "desc": "Ea est ea tempor tempor. Reprehenderit commodo duis quis in mollit quis veniam pariatur aute pariatur dolor non deserunt culpa.",
    "link": "https://www.google.com/"
  },

]

  export default apps;