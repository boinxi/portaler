import React, { useState } from 'react';
import './App.css';
import Tile from './components/tile'
import apps from './Applications'
import eggApps from './eggApps.js'
import Grid from '@material-ui/core/Grid';
import logo from './images/logo3.png'
import logoEgg from './images/logo4.png'
import Msg from './images/msg.png'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import msgs from './msgs.json'
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
}));

function App() {
  const classes = useStyles();
  let [filter, setFilter] = useState(0)
  let [appList, setAppList] = useState(apps)
  let [egg, setEgg] = useState(false)
  let filteredApps = filter ? appList.filter(i => i.name.includes(filter.toLowerCase())) : appList;

  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState(apps[0].desc);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const popoverOpen = Boolean(anchorEl);
  const popoverId = popoverOpen ? 'simple-popover' : undefined;

  function handleDialogOpen() {
    console.log({ dialogOpen });
    setDialogOpen(true);
  }

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  function handlePopoverOpen(event) {
    setAnchorEl(event.currentTarget);
  }

  function handlePopoverClose() {
    setAnchorEl(null);
  }

  console.log("egg is: " + egg)
  return (
    <div className={egg? "App bodyegg" : "App"}>
      <AppBar position="fixed" >
        <Toolbar>
          <img src={egg ? logoEgg : logo} style={{ height: '50px', padding: '20px' }} onDoubleClick={()=>{
            if(!egg) {
              setAppList(eggApps);
              setEgg(true);
            } else {
              setAppList(apps);
              setEgg(false);
            }
          }}
          className={egg ? 'appegg' : ''}/>
          <div style={{ position: 'absolute', right: '70px' }}>
            <div className={classes.search} >

              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
                onChange={(s) => { setFilter(s.target.value) }}
              />
              <span style={{position:'absolute', bottom:'-8px'}}>
              {msgBtn(handlePopoverOpen, popoverId, popoverOpen, anchorEl, handlePopoverClose)}
              </span>
            </div>
            <SimpleDialog selectedValue={selectedValue} open={dialogOpen} onClose={handleDialogClose} />
          </div>
        </Toolbar>
      </AppBar>
      <div>
        <Grid container style={{
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingTop: '80px'
        }}
          className="containerWrapper" spacing='5px'>
          {
            filteredApps.map(x =>
              <Grid item xl={3} lg={3} md={4} sm={6}
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justify="center"
                style={{ marginBottom: '20px', marginTop: '40px', width: '390px' }}
              >
                <Tile
                  key={x.id}
                  title={x.title}
                  id={x.id}
                  isActive={x.isActive}
                  img={x.img}
                  name={x.name}
                  desc={x.desc}
                  link={x.link}
                  descClick={
                    () => {
                      setSelectedValue([x.name, x.desc])
                      handleDialogOpen()
                    }
                  }
                  egg={egg}
                />
              </Grid>
            )
          }
        </Grid>
      </div>
    </div>
  );
}

export default App;

function msgBtn(handlePopoverOpen, popoverId, popoverOpen, anchorEl, handlePopoverClose) {
  return (
    <div>
      <Button aria-describedby={popoverId} onClick={handlePopoverOpen} style={{marginLeft:'5px'}}>
        <img src={Msg} style={{ width: '30px' }} />
        <a className={msgs.msg.length > 0? "msgcircle" : "msgcirclehidden"}>
          {msgs.msg.length}
        </a>
      </Button>
      <Popover
      style={{marginTop:'10px'}}
        id={popoverId}
        open={popoverOpen}
        anchorEl={anchorEl}
        onClose={handlePopoverClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <div style={{minWidth:'200px', maxWidth:'400px'}}>
          {
            msgs.msg.map((ms) => {
              return (
                <div>
                  <p style={{margin:'20px', textAlign:'center', fontSize:'20px'}}>
                    {ms}
                  </p>
                  <Divider />
                </div>
              );
            })
          }
        </div>
      </Popover>
    </div>
  );
}
function SimpleDialog(props) {
  const { onClose, selectedValue, open } = props;

  function handleClose() {
    onClose();
  }

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title" style={{ textAlign: 'center' }}>
        {selectedValue[0]}
      </DialogTitle>
      <a style={{ textAlign: 'center', margin: '20px', fontSize: '20px' }}>{selectedValue[1]}</a>
    </Dialog>
  );
}
